var fs = require('fs');
var path = require('path');

module.exports = function(app) {

    app.get('/', function(req, res) {
        res.render('../public/views/index');
    });

    app.get('/remote', function(req, res) {
        res.render('../public/views/remote');
    });

    app.get('/experiment/:id', function(req, res) {
        res.render('../public/views/experiment_' + req.params.id);
    });
};