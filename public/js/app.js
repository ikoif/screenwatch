// cache the container
var $container = $('#container');

// get the window height in order to adjust the height of
// each frame
var windowHeight = $(window).height();

// set the distance from top+bottom and centered frame
var padding = 0;

// amount of pixels the user has to scroll in order
// for a frame to change
var pixelsPerFrame = 100;

// define images
var images = [];
for(var i=0; i < imageCount; i++){
	images.push("/images/Image_sequence (" + (i+1) + ").JPG")
}

function resizer(index){
	windowHeight = $(window).height();
	// embed the style
	$('#customCss').html($('#customCss').html() + '.frame{height:'+(windowHeight-padding)+'px;}');
	if(index){
		var marginTop = index * (windowHeight-padding);
		$imageHld.css('margin-top','-'+(marginTop-padding/2)+'px')
	}
}

// create the image holder div and append inner divs for 
// each frame, insert in one go for browser render speed
var imageHld = document.createElement("div");
imageHld.id = "imageHld";
$imageHld = $(imageHld);

for(i=0; i < imageCount; i++){
	var newImage = document.createElement("div");
	newImage.classList.add("frame")
	$(newImage).css("background-image", "url('" + images[i] + "')")
	imageHld.appendChild(newImage)
}

$container.append(imageHld)

$(function() {	
	resizer();
	
	// TweenMax can tween any property of any object. We use this object to cycle through the array
	var obj = {curImg: 0};
	var lastIndex = -1;
	// create tween
	var tween = TweenMax.to(obj, 0.5,
		{
			curImg: images.length - 1,	    // animate propery curImg to number of images
			roundProps: "curImg",			// only integers so it can be used as an array index
			repeat: 0,						// repeat 0 times
			immediateRender: true,			// load first image automatically
			ease: Linear.easeNone,			// show every image the same ammount of time
			onUpdate: function () {
				if(obj.curImg != lastIndex){
					var marginTop = obj.curImg * (windowHeight-padding);

					$imageHld.css('margin-top','-'+(marginTop-padding/2)+'px');
					// $imageHld.css('transform','translateY(-'+(marginTop-padding/2)+'px)');

					setTimeout(function(){
						if( (obj.curImg + 1) <= (images.length - 1) ){
							//
						}
					}, 200)
				}

				lastIndex = obj.curImg;
			}
		}
	);

	$(window).on('resize', function(){
		resizer(obj.curImg);
	});

	// init controller
	var controller = new ScrollMagic.Controller();

	// build scene
	var scene = new ScrollMagic.Scene({triggerElement: "#trigger", duration: imageCount*pixelsPerFrame})
					.setTween(tween)
					.addIndicators() // add indicators (requires plugin)
					.addTo(controller);

	// reject modal window funcs
	function isMobile(){
		return /Mobi/.test(navigator.userAgent)
	}
	if(!isMobile()){
		$.reject({
			reject: {
				safari: 5, // Apple Safari
				chrome: 35, // Google Chrome
				firefox: 30, // Mozilla Firefox
				msie: 12, // Microsoft Internet Explorer
				opera: 30, // Opera
				konqueror: true, // Konqueror (Linux)
				unknown: true // Everything else
			},
			imagePath: '../images/browserSupport/',
			display: ['firefox','chrome','opera'], // Displays only firefox, chrome, and opera
			//- close: false,
			header: 'Знаете ли, че вашият Интернет браузър е остарял?', // Header Text,
			paragraph1: 'Интернет браузър, който използвате е остарял и има възможност да не е съвместим с Bookinfo.bg. Това е списък с най-популярните браузъри, които е препоръчително да ползвате.', // Paragraph 1
			paragraph2: 'Кликнете на иконка, за препратка към официална страниза за сваляне', //Just click on the icons to get to the download page
			closeLink: 'Продължи без обновление...', // Message below close window link,
			closeMessage: '',

		}); // Customized Browsers
	}
	return false;
});