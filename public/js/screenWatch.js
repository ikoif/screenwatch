/*

///////------_____    ScreenWatch.JS    _____------\\\\\\\

Author: Ivaylo Danev <ikoifko@gmail.com>,
Author: Vesela Chepisheva <vesitoesuper@gmail.com>



dependencies:

Tracking.js
https://github.com/eduardolundgren/tracking.js

	assets/tracking-min.js

Debug HTML elements if found:

#screenWatch-track-object
#screenWatch-track-area
#screenWatch-crossair

*/

// adding colour tracking to the tracking library and 
// creating a function for calling it once the tracker is 
// initialised in screenWatch

function initGUIControllers(tracker) {
  
  var trackedColors = {
    cyan: true,
    magenta: true,
    yellow: false,
    custom: false
  };

  tracker.customColor = '#00FF00';

  function createCustomColor(value) {
    var components = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(value);
    var customColorR = parseInt(components[1], 16);
    var customColorG = parseInt(components[2], 16);
    var customColorB = parseInt(components[3], 16);

    var colorTotal = customColorR + customColorG + customColorB;

    if (colorTotal === 0) {
      tracking.ColorTracker.registerColor('custom', function(r, g, b) {
        return r + g + b < 10;
      });
    } else {
      var rRatio = customColorR / colorTotal;
      var gRatio = customColorG / colorTotal;

      tracking.ColorTracker.registerColor('custom', function(r, g, b) {
        var colorTotal2 = r + g + b;

        if (colorTotal2 === 0) {
          if (colorTotal < 10) {
            return true;
          }
          return false;
        }

        var rRatio2 = r / colorTotal2,
          gRatio2 = g / colorTotal2,
          deltaColorTotal = colorTotal / colorTotal2,
          deltaR = rRatio / rRatio2,
          deltaG = gRatio / gRatio2;

        return deltaColorTotal > 0.9 && deltaColorTotal < 1.1 &&
          deltaR > 0.9 && deltaR < 1.1 &&
          deltaG > 0.9 && deltaG < 1.1;
      });
    }

    updateColors();
  }

  function updateColors() {
    var colors = [];

    for (var color in trackedColors) {
      if (trackedColors[color]) {
        colors.push(color);
      }
    }

    tracker.setColors(colors);
  }


  updateColors();
}




// call this on browser load in order for the 
// document.element to be accessable

var screenWatch = function(video, callback, screenLost) {


	// getting DOM refrence to the video node

	// should be passed as an argument that is
	// a JS DOM reference to a Video node
	// var video = document.getElementById('video');

	video.style.position = "absolute";
	video.style.top = "0px";
	video.style.left = "0px";
	video.style.visibility = "hidden";
	video.style.webkitFilter = "saturate(3.0)";


	// used for debugging if found
	var trackDebugElementName = 'screenWatch-track-object';
	var trackDebugElement = document.getElementById(trackDebugElementName);
	if(trackDebugElement){
		trackDebugElement.style.background = "magenta";
		trackDebugElement.style.position = "fixed";
	}

	// used for debugging if found
	var trackLimitElementName = 'screenWatch-track-area';
	var trackLimitElement = document.getElementById(trackLimitElementName);
	if(trackLimitElement){
		trackLimitElement.style.position = "fixed";
		trackLimitElement.style.top = "0px";
		trackLimitElement.style.left = "0px";
		trackLimitElement.style.width = video.width + "px";
		trackLimitElement.style.height = video.height + "px";
		trackLimitElement.style.border = '1px solid black';
	}

	// used for crossair debugging if found
	var crossAirElement = document.getElementById('screenWatch-crossair');
	if(crossAirElement){
		crossAirElement.style.position = "fixed";
		crossAirElement.style.top = "px";
		crossAirElement.style.left = "0px";
		crossAirElement.style.width = "1px";
		crossAirElement.style.height = "1px";
		crossAirElement.style.background = "rgb(255,0,0)";

		var handleStlye = function(div, h, l){
			div.style.position = "absolute";
			div.style.top = "0px";
			div.style.left = "0px";
			if(h){
				div.style.width = "10px";
				div.style.height = "1px";
				if(l){
					div.style.marginLeft = "-11px";
				}else{
					div.style.marginRight = "1px";
				}
			}else{
				div.style.width = "1px";
				div.style.height = "10px";
				if(l){
					div.style.marginTop = "-11px";
				}else{
					div.style.marginTop = "1px";
				}
			}
			div.style.background = "rgb(255,255,255)";

		}

		var i=0;
		
		for(i;i<4;i++){
			var element = document.createElement('div');
			crossAirElement.appendChild(element);
			handleStlye(element, i === 0 || i === 1, i === 0 || i === 2);
		}
	}


	
	var trackerWidth = video.width;
	var trackerHeight = video.height;
	var tolerance = 0.2;
	var sampling = 8;

	var totalWindowSize = trackerHeight + trackerWidth;
	var tracker = new tracking.ColorTracker();

	var lastValues = [{
		leftToRight: 0,
		topToBottom: 0,
		zoomLevel: 0
	}];

	var lastValuesSum = {}

	// tell tracking.js which element to track and add webcamera 
	// to the video element
	tracking.track(video.id, tracker, {
		camera: true
	});

	tracker.on('track', function(event) {

		if (event.data.length) {

			rectSum = {}
			var rect = event.data;

			rect.forEach(function(e, i){
				rectSum.width = !i ? e.width : rectSum.width += e.width
				rectSum.height = !i ? e.height : rectSum.height += e.height
				rectSum.x = !i ? e.x : rectSum.x += e.x
				rectSum.y = !i ? e.y : rectSum.y += e.y
				rectSum.color = e.color;
			})

			rectSum.width = rectSum.width / rect.length;
			rectSum.height = rectSum.height / rect.length;
			rectSum.x = rectSum.x / rect.length;
			rectSum.y = rectSum.y / rect.length;


			if (rectSum.color === 'magenta' || rectSum.color === 'cyan') {
				var rectSize = rectSum.width + rectSum.height;
				var zoomLevel = (rectSize / totalWindowSize) * 2;
				var leftToRight = (rectSum.x + rectSum.width/2) / trackerWidth;
				var topToBottom = (rectSum.y + rectSum.height/2) / trackerHeight;

				if(trackDebugElement){
					trackDebugElement.style.left = rectSum.x + 'px';
					trackDebugElement.style.top = rectSum.y + 'px';
					trackDebugElement.style.width = rectSum.width + 'px';
					trackDebugElement.style.height = rectSum.height + 'px';
				}

				// element.innerHTML = 'zoom level: ' + zoomLevel + 'left to right value: ' + leftToRight;

				lastValuesSum.leftToRight = leftToRight;
				lastValuesSum.topToBottom = topToBottom;
				lastValuesSum.zoomLevel = zoomLevel;

				lastValues.forEach(function(e, i) {
					lastValuesSum.leftToRight = !i ? e.leftToRight : lastValuesSum.leftToRight += e.leftToRight
					lastValuesSum.topToBottom = !i ? e.topToBottom : lastValuesSum.topToBottom += e.topToBottom
					lastValuesSum.zoomLevel = !i ? e.zoomLevel : lastValuesSum.zoomLevel += e.zoomLevel
				})

				lastValuesSum.leftToRight = lastValuesSum.leftToRight / lastValues.length
				lastValuesSum.topToBottom = lastValuesSum.topToBottom / lastValues.length
				lastValuesSum.zoomLevel = lastValuesSum.zoomLevel / lastValues.length

				if (lastValues.length > sampling) lastValues.splice(0, 1);

				lastValues.push({
					leftToRight: leftToRight,
					topToBottom: topToBottom,
					zoomLevel: zoomLevel
				});



				if(crossAirElement){
					crossAirElement.style.left = leftToRight * video.width + 'px';
					crossAirElement.style.top = topToBottom * video.height + 'px';
				}

				// console.log(lastValuesSum.leftToRight, lastValuesSum.topToBottom)

				// controls.mouseX = lastValuesSum.leftToRight;
				// controls.mouseY = lastValuesSum.topToBottom;
				// mouseY = (lastValuesSum.topToBottom * window.innerHeight - window.innerHeight/2 ) * 10
				// console.log(lastValuesSum)
				// minzoomspeed = lastValuesSum.zoomLevel;

				callback(lastValuesSum)
			}
		} else {
			screenLost();
		};
	});

	initGUIControllers(tracker);
};