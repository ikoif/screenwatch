![screenWatch logo](https://bytebucket.org/ikoif/screenwatch/raw/99569c1f3486d46401d13bff9f23beb5859ee550/public/images/logo.jpg)

## Welcome to our experiment.

We (Ivaylo Danev (programmer) & Vesela Chepisheva  ) have created a little utility that translates camera detected motion to mouse movement (ideally from a mobile device's screen). It is a simple library that can be used as a controller for just about anything.

### Live demo here (you need a mobile smart phone seperately) https://screenwatchjs.herokuapp.com/

## To run the experiment locally:

- clone the repo
- make sure you have nodejs installed (https://nodejs.org/en/)
- $ npm install
- $ npm start
- the experiment should be running on http://localhost:8080
- enjoy :)

## API and Guide

To learn how to use the controller, check out the file in `app root/public/views/experiment_2.html` or `app root/public/views/experiment_2.html`

The main file in this application that delivers an easy to use JavaScript object `public/js/screenWatch.js`


## __screenWatch(_reference to canvas DOM element_, callback(___object___))__

#canvas

The function `screenWatch` expects a reference to a canvas DOM element. For example `document.getElementById('videoCanvas')`

The next examples are considered in the same conditions where the webcamera is right above the screen it is viewed on and the mobile device's screen being right in front of the camera pointed at it. 
The ___object___ that is passed in the callback function has the following properties:

#leftToRight
A float value from 0.0 to 1.0. It represents the position of the detected object from left to right horizontally (x). 0.0 being completely left and 1.0 being completely right.

#topToBottom
A float value from 0.0 to 1.0. It represents the position of the detected object from top to bottom vertically (y). 0.0 being completely top and 1.0 being completely bottom.

#zoomLevel
A float value from 0.0 to 1.0. It represents the distance of the detected object from the screen (z). The closer the screen is, the higher the value, so 1.0 is when the whole detection area is covered by the colour it needs to track, so it is almost inconsiderable to reach (based on tests). 0.0 is when the device is the furthest.